//
//  ViewController.swift
//  Dice
//
//  Created by John Daryl Baltazar on 15/08/2017.
//  Copyright © 2017 John Daryl Baltazar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var randomDiceNumber1 : Int = 0
    var randomDiceNumber2 : Int = 0
    
    let diceListImage = ["dice1", "dice2", "dice3", "dice4", "dice5", "dice6"]
    
    @IBOutlet weak var diceImage1: UIImageView!
    @IBOutlet weak var diceImage2: UIImageView!
    
    override func viewDidLoad() {
        updateDiceValue()
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func rollImage(_ sender: UIButton) {
        updateDiceValue()
    }
    
    func updateDiceValue () {
        
        randomDiceNumber1 = Int(arc4random_uniform(6))
        randomDiceNumber2 = Int(arc4random_uniform(6))
        
        diceImage1.image = UIImage(named: diceListImage[randomDiceNumber1])
        diceImage2.image = UIImage(named: diceListImage[randomDiceNumber2])
        
    }

}

